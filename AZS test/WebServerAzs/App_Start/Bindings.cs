﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using Data.DAL;
using Data.Entities;
using Data.Security;
using Data.Services;
using SimpleInjector;
using SimpleInjector.Integration.Web;

namespace WebServerAzs.App_Start
{
    public class Bindings
    {
        private static readonly Lazy<Container> _container = new Lazy<Container>(CreateContainer);

        public static void Bind(Container container)
        {            
            
            container.Register<IRepository<GasStation>, Repository<GasStation>>();      

            container.Register<IUserRepository, UserRepository>();

            container.Register<IGasStationService, GasStationService>();
        }

        public static Container CreateContainer()
        {
            var container = new Container();            

            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration, Assembly.GetExecutingAssembly());


            Bind(container);

            container.Options.SuppressLifestyleMismatchVerification = true;

            return container;
        }

        public static Container GetContainer()
        {
            return _container.Value;
        }




    }



}