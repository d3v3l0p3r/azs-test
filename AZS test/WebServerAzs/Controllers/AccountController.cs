﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Data.Security;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace WebServerAzs.Controllers
{
    [RoutePrefix("api/account")]
    public class AccountController : ApiController
    {
        private ApplicationSignInManager _signInManager;
        private DataUserManager _userManager;

        public AccountController()
        {

        }


        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public DataUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<DataUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }



        [HttpPost]
        [AllowAnonymous]
        [System.Web.Mvc.ValidateAntiForgeryToken]
        [Route("login")]

        public async Task<IHttpActionResult> Login(string login, string password)
        {
            var result = await SignInManager.PasswordSignInAsync(login, password, true, false);

            if (result == SignInStatus.Success)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
