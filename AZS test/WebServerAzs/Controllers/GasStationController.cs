﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Data.Entities;
using Data.Services;

namespace WebServerAzs.Controllers
{
    [Authorize]
    [RoutePrefix("api/gasstation")]
    public class GasStationController : ApiController
    {
        private readonly IGasStationService _gasStationService;

        public GasStationController(IGasStationService gasStationService)
        {
            _gasStationService = gasStationService;
        }


        [Route("all")]
        public IEnumerable<GasStation> GetStations(int take = 10, int skip = 0)
        {
            if (take > 100)
            {
                throw new Exception("");
            }

            if (take < 0)
            {
                throw new Exception("");
            }

            var result = _gasStationService.GetAll().OrderByDescending(x => x.ID).Skip(skip).Take(take).ToList();

            return result;
        }

        [Route("total")]
        public int GetTotal()
        {
            int total = _gasStationService.GetAll().Count();
            return total;
        }

        [HttpPut]
        [Route("update")]
        public IHttpActionResult Update(GasStation station)
        {
            try
            {
                _gasStationService.Update(station);
                return Ok();
            }
            catch (Exception error)
            {
                return InternalServerError(error);
            }
        }


        [HttpDelete]
        [Route("delete")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _gasStationService.Delete(id);
                return Ok();
            }
            catch (Exception error)
            {
                return InternalServerError(error);
            }
        }
    }
}