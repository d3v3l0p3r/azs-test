﻿using Microsoft.Owin;
using Owin;


[assembly: OwinStartup(typeof(WebServerAzs.Startup))]

namespace WebServerAzs
{
    public partial class Startup
    {
  

        public void Configuration(IAppBuilder app)
        {
          ConfigureAuth(app);
        }
    }
}
