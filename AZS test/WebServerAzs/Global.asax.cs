﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using Data.DAL;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using WebServerAzs.App_Start;

namespace WebServerAzs
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {

            var dataContext = new DataContext();
            
            


            var container = Bindings.GetContainer();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

            
            
            
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);            
        }
    }
}