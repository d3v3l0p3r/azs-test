﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;
using Newtonsoft.Json;

namespace AszClient.WebClient
{
    public class AzsWebClient
    {
        private Dictionary<string, string> _tokenDictionary;
        private readonly HttpClient _httpClient;

        public AzsWebClient()
        {

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri("http://localhost:40000/"),
            };
            _httpClient.DefaultRequestHeaders.Accept.Clear();
        }

        public bool Connect(string login, string password)
        {
            var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>( "grant_type", "password" ),
                    new KeyValuePair<string, string>( "username", login ),
                    new KeyValuePair<string, string> ( "Password", password )
                };
            var content = new FormUrlEncodedContent(pairs);

            var response = _httpClient.PostAsync("Token", content).Result;


            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;

                _tokenDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(result);

                _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", _tokenDictionary["access_token"]);

                return true;
            }
            else
            {
                return false;
            }
        }


        public async Task<IEnumerable<GasStation>> GetStations(int take = 10, int page = 0)
        {

            int skip = --page * take;
            string reqUrl = $"api/gasstation/all?take={take}&skip={skip}";

            var response = await _httpClient.GetAsync(reqUrl);

            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsAsync<IEnumerable<GasStation>>();

                return result;
            }

            throw new Exception("Что-то пошло не так =)");
        }

        public int GetTotal()
        {
            var response = _httpClient.GetAsync("api/gasstation/total").Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<int>().Result;
            }

            return 0;
        }

        public bool UpdateStation(GasStation gasStation)
        {
            var response = _httpClient.PutAsync("api/gasstation/update", gasStation, new JsonMediaTypeFormatter()).Result;

            return response.IsSuccessStatusCode;            
        }

        public bool DeleteStation(int stationId)
        {
            var response = _httpClient.DeleteAsync($"api/gasstation/delete?id={stationId}");

            return response.Result.IsSuccessStatusCode;
        }
    }
}
