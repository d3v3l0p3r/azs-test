﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using AszClient.WebClient;
using Data.Entities;

namespace AszClient
{
    public partial class MainForm : Form
    {
        private readonly AzsWebClient _client;
        private int pageSize = 30;
        private int page = 1;
        private int total = 0;


        public MainForm()
        {
            InitializeComponent();
            _client = new AzsWebClient();
           
            Shown += MainForm_Shown;
            dataGridView1.CellContentDoubleClick += DataGridView1_CellContentDoubleClick;
        }

        private void DataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var data = dataGridView1.Rows[e.RowIndex].DataBoundItem as GasStation;

            if (data != null)
            {
                UpdateGasStation(data);
            }

        }

        private void UpdateGasStation(GasStation station)
        {
            var editForm = new AszForm(station);
            var dialogResult = editForm.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                var result = _client.UpdateStation(station);

                if (result)
                {
                    ReadStations();
                }
            }
        }

        private void CreateStation()
        {
            var station = new GasStation();
            var editForm = new AszForm(station);
            var dialogResult = editForm.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                var result = _client.UpdateStation(station);

                if (result)
                {
                    ReadStations();
                }
            }
        }

        private void DeleteStation(GasStation station)
        {
            var result = _client.DeleteStation(station.ID);
            if (result)
            {
                ReadStations();
            }
        }
        private void MainForm_Shown(object sender, EventArgs e)
        {
            var authForm = new AuthForm(_client);
            var dialogResult = authForm.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                authForm.Close();
                InitData();
            }
            else
            {
                this.Close();
            }
        }

        private async void ReadStations()
        {
            var data = await _client.GetStations(pageSize, page);

            total = _client.GetTotal();

            gasStationBindingSource.DataSource = data;
        }

        private void InitData()
        {
            total = _client.GetTotal();

            pageLabel.Text = $"страница {page} из {total / pageSize}";

            ReadStations();
        }

        private void nextPageButton_Click(object sender, EventArgs e)
        {
            if (page < total / pageSize)
            {
                page++;
                ReadStations();
                pageLabel.Text = $"страница {page} из {total / pageSize}";
            }
        }

        private void prevPageButton_Click(object sender, EventArgs e)
        {
            if (page > 1)
            {
                page--;
                ReadStations();
                pageLabel.Text = $"страница {page} из {total / pageSize}";
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CreateStation();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            var gs = GetSelectedGasStation();
            if (gs != null)
            {
                UpdateGasStation(gs);
            }

        }

        private void dataGridView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var ht = dataGridView1.HitTest(e.X, e.Y);

                if (ht.Type == DataGridViewHitTestType.Cell)
                {
                    dataGridView1.Rows[ht.RowIndex].Selected = true;
                    contextMenuStrip1.Show(MousePosition);
                }
            }
        }

        private GasStation GetSelectedGasStation()
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                return dataGridView1.SelectedRows[0].DataBoundItem as GasStation;
            }

            return null;
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            var gs = GetSelectedGasStation();
            if (gs != null)
            {
                DeleteStation(gs);
            }
        }
    }
}
