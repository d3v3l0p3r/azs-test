﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AszClient.WebClient;

namespace AszClient
{
    public partial class AuthForm : Form
    {
        public string Login { get; set; } = "UserName1";
        public string Password { get; set; } = "123123";

        private readonly AzsWebClient _client;
        

        public AuthForm(AzsWebClient client)
        {
            InitializeComponent();

            _client = client;

            this.LoginTextBox.DataBindings.Add(new Binding("Text", this, "Login"));
            this.PasswordTextBox.DataBindings.Add(new Binding("Text", this, "Password"));
        }

        private void applyButton_Click(object sender, EventArgs e)
        {
            if (TryConnect())
            {
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Не удалось авторизоваться");
            }
        }

        private bool TryConnect()
        {
            var connectResult = _client.Connect(Login, Password);

            return connectResult;
        }
    }
}
