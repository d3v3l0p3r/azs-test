﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Data.Entities;

namespace AszClient
{
    public partial class AszForm : Form
    {
        public GasStation Station { get; set; }

        public AszForm(GasStation station)
        {
            InitializeComponent();
            Station = station;
            nameTextBox.DataBindings.Add(new Binding("Text", Station, "Name"));
            addressTextBox.DataBindings.Add(new Binding("Text", Station, "Address"));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
    }
}
