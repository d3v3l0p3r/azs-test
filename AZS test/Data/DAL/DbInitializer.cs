﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;
using Data.Security;
using Microsoft.AspNet.Identity;

namespace Data.DAL
{
    public class DbInitializer : CreateDatabaseIfNotExists<DataContext>
    {
        protected override void Seed(DataContext context)
        {
            base.Seed(context);

            var um = new DataUserManager(UserRepository.Create());

            for (int i = 0; i < 100; i++)
            {
                string userName = "UserName";

                var user = new User()
                {
                    UserName = userName + i,
                    Email = userName + i + "@mail.com",
                    EmailConfirmed = true
                };

               var result = um.Create(user, "123123");
            }

            for (int i = 0; i < 1000; i++)
            {
                var gasStation = new GasStation
                {
                    Name = "Station " + i,
                    Address = "addres"
                };

                context.GasStations.Add(gasStation);
                context.SaveChanges();
            }
        }

       
    }
}
