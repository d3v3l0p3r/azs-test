using System.Linq;
using Data.Entities;

namespace Data.DAL
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        private readonly DataContext _dataContext;

        public Repository()
        {
            _dataContext = new DataContext();
        }

        public T Get(int id)
        {
            var dbSet = _dataContext.Set<T>();
            return dbSet.Find(id);
        }

        public void Delete(T entity)
        {
            var dbSet = _dataContext.Set<T>();
            dbSet.Remove(entity);
            _dataContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var dbSet = _dataContext.Set<T>();
            var entity = dbSet.Find(id);
            Delete(entity);
        }

        public T Update(T entity)
        {
            var dbSet = _dataContext.Set<T>();

            dbSet.Attach(entity);

            _dataContext.Entry<T>(entity).State = System.Data.Entity.EntityState.Modified;

            _dataContext.SaveChanges();

            return entity;
        }

        public IQueryable<T> GetAll()
        {
            var dbSet = _dataContext.Set<T>();
            return dbSet;
        }

        public T Create(T entity)
        {
            var dbSet = _dataContext.Set<T>();

            dbSet.Add(entity);

            _dataContext.SaveChanges();

            return entity;
        }
    }
}