﻿
using System.Data.Entity;
using Data.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;


namespace Data.DAL
{
    public class DataContext : IdentityDbContext<User, Role, int, Login, UserRole, Claim>
    {        
        public DataContext() : base(nameof(DataContext))
        {
            
            if (Database.Exists())
            {
                Database.SetInitializer(new MigrateDatabaseToLatestVersion<DataContext, EFContextConfiguration>());
            }
            else
            {
                Database.SetInitializer(new DbInitializer());
                Database.Initialize(true);
            }

            Configuration.LazyLoadingEnabled = true;
            
        }
                
        public DbSet<GasStation> GasStations { get; set; }

        public static void Init()
        {

        }

        public static DataContext Create()
        {
            return new DataContext();
        }

    }
}
