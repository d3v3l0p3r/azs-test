﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;

namespace Data.DAL
{
    public interface IRepository<T> where T : Entity
    {
        T Create(T intity);
        T Get(int id);
        void Delete(T entity);
        void Delete(int id);
        T Update(T entity);
        IQueryable<T> GetAll();
    }
}
