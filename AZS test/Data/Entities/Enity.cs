﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Entity
    {
        [Key]
        public int ID { get; set; }

        
        public bool Hidden { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
