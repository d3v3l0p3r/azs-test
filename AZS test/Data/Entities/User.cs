﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Data.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;


namespace Data.Entities
{
    public class User : IdentityUser<int, Login, UserRole, Claim>
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(DataUserManager manager, string authenticationType)
        {         
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
         
            return userIdentity;
        }
    }

    public class Login : IdentityUserLogin<int>
    {

    }

    public class Claim : IdentityUserClaim<int>
    {
    }

    public class UserRole : IdentityUserRole<int>
    {
    }

    public class Role : IdentityRole<int, UserRole>
    {

    }
}
