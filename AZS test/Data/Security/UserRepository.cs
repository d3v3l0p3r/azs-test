﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.DAL;
using Data.Entities;
using Data.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Data.Security
{
    public interface IUserRepository : IUserStore<User, int>
    {
        IUserRepository Create();
    }

    public class UserRepository : UserStore<User, Role, int, Login, UserRole, Claim>, IUserRepository
    {
        public UserRepository(DataContext dataContext)
            : base(dataContext)
        {

        }



        public static UserRepository Create()
        {
            return new UserRepository(new DataContext());
        }

        IUserRepository IUserRepository.Create()
        {
            return Create();
        }
    }
}
