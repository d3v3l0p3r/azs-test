﻿using System.Linq;
using Data.Entities;

namespace Data.Services
{
    public interface IBaseService<T> where T : Entity
    {
        T Create(T entity);
        T Get(int id);
        IQueryable<T> GetAll();
        T Update(T entity);
        void Delete(int id);
    }
}