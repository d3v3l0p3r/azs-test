﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.DAL;
using Data.Entities;

namespace Data.Services
{

    public class BaseService<T> : IBaseService<T> where T : Entity
    {
        private readonly IRepository<T> _repository;

        public BaseService(IRepository<T> repository)
        {
            _repository = repository;
        }


        public virtual T Create(T entity)
        {
            return _repository.Create(entity);
        }

        public virtual T Get(int id)
        {
            return _repository.Get(id);
        }

        public virtual IQueryable<T> GetAll()
        {
            return _repository.GetAll();
        }

        public virtual T Update(T entity)
        {
            if (entity.ID == 0)
            {
                return Create(entity);
            }

            return _repository.Update(entity);
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }
    }
}
