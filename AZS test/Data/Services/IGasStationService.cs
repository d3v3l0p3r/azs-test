﻿using Data.Entities;

namespace Data.Services
{
    public interface IGasStationService : IBaseService<GasStation>
    {
        
    }
}